<?php
require_once("lib/starter.php");
has_access();

require_once("lib/phpmailer/PHPMailerAutoload.php");

if(!isset($_REQUEST["id"])){
	my_header("index.php?warn=Nelze potvrdit nezadanou akci","Must supply ID");
}
if(dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:plains] WHERE [id_plains]=%i",$_REQUEST["id"]) != 1){
	my_header("index.php?err=Toto hlasování neexistuje","ID invalid");
}

//loading poll data
$plain_data = dibi::query("SELECT [id_plains] AS id, [subject],[content],[sent],[file1],[file2],[file3] FROM [:sh:plains] WHERE [id_plains]=%i",$_REQUEST["id"])->setFormat(dibi::DATE, 'j.n.Y')->setFormat(dibi::DATETIME,'H:i j.n.Y')->fetch();

/**********action approvalrequest**********/
if($_REQUEST["action"] == "approvalrequest"){
  die("NotImplementedError");
}
/******** action sendplain ***********/
if($_REQUEST["action"] == "sendplain" and isset($_REQUEST["id"])){
    if (!isset($_REQUEST["sendgroup"])) {
        my_header("submitplain.php?err=Musíte vybrat skupinu, které bude email odeslán&id=" . $_REQUEST["id"], "No group supplied");
    }
    //get recipients (children IDs, nicks and emails)
    //using GROUP_CONCAT for finding siblings and sending 1 mail with multiple links
    if ($_REQUEST["sendgroup"] == "all") {
        $recipients = dibi::query("SELECT GROUP_CONCAT([id_children]) AS ids, GROUP_CONCAT([nick]) AS nicks, [contact_email] FROM [:sh:children] WHERE [active]=1 GROUP BY [contact_email]");
    } else {
        $recipients = dibi::query("SELECT GROUP_CONCAT([id_children]) AS ids, GROUP_CONCAT([nick]) AS nicks,[contact_email] FROM [:sh:children] WHERE [active]=1 AND [id_children] IN (SELECT [child] FROM [:sh:group_members] WHERE [group]=%i) GROUP BY [contact_email]", $_REQUEST["sendgroup"]);
    }
    if (count($recipients) == 0) {
        my_header("submitpoll.php?err=Zadaná skupina nemá nikoho, komu by šel poslat email&id=" . $_REQUEST["id"], "No emails in selected group");
    }

    $failed_recipients = Array();
    foreach($recipients as $recipient){
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);

        if($sh_config["smtp_on"]){
            $mail->isSMTP();
            $mail->Host = $sh_config["smtp_host"];
            $mail->Port = $sh_config["smtp_port"]; //587 for Google
            $mail->SMTPAuth = true;
            $mail->Username = $sh_config["smtp_user"];
            $mail->Password = $sh_config["smtp_pass"];
            $mail->SMTPSecure = 'tls';
        }
        else{
            $mail->isMail();
        }
        $mail->Subject = $sh_config["send_plains_subject"]." ".$plain_data["subject"];
        $mail->setFrom(strip_tags($sh_config["send_emails_from"]));
        $mail->addReplyTo(strip_tags($sh_config["send_emails_reply_to"]));
        //add recipients
        foreach(explode(',',$recipient["contact_email"]) as $single_rec){
            $mail->addAddress($single_rec);
        }
        //if bcc is enabled
        if(!empty($sh_config["bcc_to"])){
            foreach(explode(',',$sh_config["bcc_to"]) as $single_rec){
                $mail->addBCC($single_rec);
            }
        }

        $template = $twig->loadTemplate("email.html");
        $mail->Body = $template->render(Array("content" => $plain_data["content"], "domain_url" => WEBURL, "email"=>$recipient["contact_email"]));
        $mail->AltBody = $plain_data["content"];
        foreach(Array($plain_data["file1"],$plain_data["file2"],$plain_data["file3"]) as $attachment){
            if(empty($attachment)){
                continue;
            }
            $mail->addAttachment($attachment);
        }

        if(!$mail->send()){
            $failed_recipients[] = $recipient["contact_email"];
        }
    }
    dibi::query("UPDATE [:sh:plains] SET [sent]=NOW() WHERE [id_plains]=%i", $_REQUEST["id"]);
    if(!empty($failed_recipients)){
        my_header("listplains.php?err=Nepodařilo se odeslat následujícím emailům".implode(',',$failed_recipients));
    }
    my_header("listplains.php?ok=Úspěšně odesláno");

}
//end of sendpoll action




$groups = dibi::query("SELECT [id_groups],[name] FROM [:sh:groups]");

//render template
$template = $twig->loadTemplate("submitplain.html");
$template->display(Array("plain_data" => $plain_data, "groups" => $groups));
