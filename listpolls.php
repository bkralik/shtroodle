<?php
require_once("lib/starter.php");
has_access();

if(isset($_REQUEST["action"]) && $_REQUEST["action"] == "adminvote" && isset($_REQUEST["id"])){
	$sent = dibi::fetchSingle("SELECT [sent] FROM [:sh:polls] WHERE [id_polls]=%i",$_REQUEST["id"]);
	if(is_null($sent)){
		my_header("listpolls.php?err=Tato akce neexistuje nebo ještě nebyla odeslána");
	}

	$votes_data = dibi::query("SELECT [id_votes] AS id, CONCAT(COALESCE([nick],' '),' (',COALESCE([lastname], ' '),')') AS člen, IF([vote] IS NOT NULL,1,0) AS odhlasováno, [updated] AS 'naposledy změněno',[key] FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [poll]=%i",$_REQUEST["id"])->setFormat(dibi::DATETIME,'H:i j.n.Y');
	$template = $twig->loadTemplate("listpolls.adminvote.html");
	$template->display(Array("votes_data" => $votes_data, "request_uri" => urlencode($_SERVER["REQUEST_URI"])));
	exit;
}




$polls_data = dibi::query("SELECT [id_polls] AS [id], [name] AS jméno, [date_start] AS začátek, [date_end] AS konec, [sent] AS odesláno, [sent] AS 'bylo odesláno' FROM [:sh:polls] ORDER BY [id_polls] DESC")->setFormat(dibi::DATE,"j.n.Y")->setFormat(dibi::DATETIME,"H:i j.n.Y");

$template = $twig->loadTemplate("listpolls.html");
$template->display(Array("polls_data" => $polls_data));



