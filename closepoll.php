<?php
require_once("lib/starter.php");
has_access();

$id = $_REQUEST['id'];

$sent = dibi::fetchSingle('SELECT [sent] FROM [:sh:polls] WHERE [id_polls]=%i',$id);
if (is_null($sent) or empty($id)){
    my_header("listpolls.php?err=Tato akce neexistuje nebo ještě nebyla odeslána");
}


//Akce closepoll
if($_REQUEST['action'] == "closepoll"){
    //delete old closepoll data
    dibi::query('DELETE FROM [:sh:turnout] WHERE [poll]=%i', $id);
    foreach($_REQUEST['participants'] as $v){
        dibi::query("INSERT INTO [:sh:turnout] ([poll], [child]) VALUES (%i,%i)", $id, $v);
    }

    my_header("closepoll.php?id=".$id.'&ok=Účast zadána');
}

//Default action

$been_closed = dibi::query('SELECT COUNT(*) AS cnt, MIN([created]) AS creation FROM [:sh:turnout] WHERE poll=%i GROUP BY [poll]',$id)->setFormat(dibi::DATE,"j.n.Y")->setFormat(dibi::DATETIME,"H:i j.n.Y")->fetch();
if($been_closed["cnt"] > 0){ //if it has been already closed
    $tmp_turnout = dibi::query('SELECT [child] FROM [:sh:turnout] WHERE [poll]=%i',$id)->fetchAll();
}
else{ // hasn't been ever closed, load from votes
    $tmp_turnout = dibi::query('SELECT [child] FROM [:sh:votes] WHERE [poll]=%i AND [vote]=1',$id)->fetchAll();
}

$turnout_data = Array();
foreach($tmp_turnout as $v){
    $turnout_data[] = $v["child"];
}

$poll_data = dibi::query("SELECT * FROM [:sh:polls] WHERE [id_polls]=%i",$id)->setFormat(dibi::DATE,"j.n.Y")->setFormat(dibi::DATETIME,"H:i j.n.Y")->fetch();
$members_data = dibi::query('SELECT [id_children] AS id, CONCAT(COALESCE([nick], " ")," (",COALESCE([lastname], " "),")") AS nickname  FROM [:sh:children] WHERE [active]=1')->fetchAll();


$template = $twig->loadTemplate("closepoll.html");
$template->display(Array("poll_data" => $poll_data, "members_data" => $members_data, 'turnout_data' => $turnout_data, "been_closed" => $been_closed ));