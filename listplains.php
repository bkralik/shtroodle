<?php
require_once("lib/starter.php");
has_access();

$plains_data = dibi::query("SELECT id_plains AS id, subject AS předmět, LEFT(content,255) AS obsah,
  ABS(((ISNULL(file1) + ISNULL(file2) + ISNULL(file3))-3)) AS přílohy, sent AS odesláno, sent AS datum
FROM [:sh:plains] ORDER BY [id_plains] DESC")->setFormat(dibi::DATETIME,'H:i j.n.Y')->fetchAll();

$template = $twig->loadTemplate("listplains.html");
$template->display(Array("plains_data" => $plains_data));



