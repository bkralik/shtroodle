BEGIN;

ALTER TABLE `sh_equipment` ALTER COLUMN `hidden` SET DEFAULT 0;

INSERT INTO sh_dbversion (version) VALUES (9;

COMMIT;