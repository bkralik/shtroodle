BEGIN;
ALTER TABLE `sh_children`
  ADD `photo_file` VARCHAR(100) DEFAULT NULL AFTER `date_member`;

INSERT INTO sh_dbversion (version) VALUES (3);
COMMIT;