BEGIN;

CREATE TABLE IF NOT EXISTS `sh_plains` (
  `id_plains` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` TEXT NOT NULL,
  `content` TEXT NOT NULL,
  `file1` TEXT NULL,
  `file2` TEXT NULL,
  `file3` TEXT NULL,
  `sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id_plains`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sh_dbversion (version) VALUES (5);

COMMIT;
