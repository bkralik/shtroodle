BEGIN;
ALTER TABLE `sh_children`
  ADD `birthday` DATE DEFAULT NULL AFTER `birthno`,
  ADD `psc` VARCHAR(16) DEFAULT NULL AFTER `address`,
  ADD `town` VARCHAR(16) DEFAULT NULL AFTER `address`,
  ADD `left_year` INT DEFAULT NULL AFTER `date_promise`;

INSERT INTO sh_dbversion (version) VALUES (4);
COMMIT;