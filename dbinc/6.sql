BEGIN;

ALTER TABLE `sh_children`
  ADD `googledrive` TEXT DEFAULT NULL AFTER `father_phone`;

INSERT INTO sh_dbversion (version) VALUES (6);

COMMIT;