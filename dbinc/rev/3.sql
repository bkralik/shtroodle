BEGIN;

ALTER TABLE `sh_children` DROP COLUMN `photo_file`;

DELETE FROM `sh_dbversion` WHERE `version`=3;


COMMIT;