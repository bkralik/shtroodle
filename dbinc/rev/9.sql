BEGIN;

ALTER TABLE `sh_equipment` ALTER COLUMN `hidden` DROP DEFAULT;

DELETE FROM `sh_dbversion` WHERE `version`=9;

COMMIT;