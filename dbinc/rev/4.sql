BEGIN;

ALTER TABLE `sh_children` DROP COLUMN `psc`;
ALTER TABLE `sh_children` DROP COLUMN `birthday`;
ALTER TABLE `sh_children` DROP COLUMN `town`;
ALTER TABLE `sh_children` DROP COLUMN `left_year`;

DELETE FROM `sh_dbversion` WHERE `version`=4;

COMMIT;