BEGIN;

DROP TABLE `sh_writeuptypes`;
DROP TABLE `sh_childrendata`;

ALTER TABLE `sh_children` DROP COLUMN `firstname`;
ALTER TABLE `sh_children` DROP COLUMN `lastname`;
ALTER TABLE `sh_children` DROP COLUMN `address`;
ALTER TABLE `sh_children` DROP COLUMN `contact_phone`;
ALTER TABLE `sh_children` DROP COLUMN `birthno`;
ALTER TABLE `sh_children` DROP COLUMN `mother_name`;
ALTER TABLE `sh_children` DROP COLUMN `mother_phone`;
ALTER TABLE `sh_children` DROP COLUMN `father_name`;
ALTER TABLE `sh_children` DROP COLUMN `father_phone`;
ALTER TABLE `sh_children` DROP COLUMN `date_member`;
ALTER TABLE `sh_children` DROP COLUMN `date_promise`;

DELETE FROM `sh_dbversion` WHERE `version`=2;

COMMIT;