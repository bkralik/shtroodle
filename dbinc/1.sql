BEGIN;

CREATE TABLE IF NOT EXISTS `sh_dbversion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `version` int(11) unsigned NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO sh_config VALUES
('bcc_to', ''),
('smtp_on', ''),
('smtp_user', ''),
('smtp_pass', ''),
('smtp_host', ''),
('smtp_port', '');

INSERT INTO sh_dbversion (version) VALUES (1);

COMMIT;
