<?php
require_once("lib/starter.php");

if($_SESSION["auth"] != 1){
	$data = dibi::fetch("SELECT [id_polls],[name],
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=1 AND [poll]=[id_polls]) AS confirmed,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=0 AND [poll]=[id_polls]) AS rejected,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote] IS NULL AND [poll]=[id_polls]) AS undecided,
		(SELECT [confirmed]+[rejected]+[undecided]) AS progress_full,
		(SELECT [confirmed]/[progress_full]*100) AS progress_confirm,
		(SELECT [rejected]/[progress_full]*100) AS progress_reject,
		(SELECT [undecided]/[progress_full]*100) AS progress_undecide
		FROM [:sh:polls] ORDER BY [sent] DESC LIMIT 1
	");
	$template = $twig->loadTemplate('index.anon.html');
	$template->display(array("data" => $data));
	exit;

}
$offset = isset($_REQUEST["off"]) ? $_REQUEST["off"] : 0;
if(isset($_REQUEST["id"])){
	$lastdata = dibi::query("SELECT [id_polls],[name],[sent],
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=1 AND [poll]=[id_polls]) AS confirmed,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote]=1 AND [poll]=[id_polls]) AS confirmed_list,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=0 AND [poll]=[id_polls]) AS rejected,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick],' '),' (',COALESCE([lastname],' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote]=0 AND [poll]=[id_polls]) AS rejected_list,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote] IS NULL AND [poll]=[id_polls]) AS undecided,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote] IS NULL AND [poll]=[id_polls]) AS undecided_list,
		(SELECT [confirmed]+[rejected]+[undecided]) AS progress_full,
		(SELECT [confirmed]/[progress_full]*100) AS progress_confirm,
		(SELECT [rejected]/[progress_full]*100) AS progress_reject,
		(SELECT [undecided]/[progress_full]*100) AS progress_undecide
		FROM [:sh:polls] WHERE [id_polls]=%i",$_REQUEST["id"])->setFormat(dibi::DATETIME,"H:i j.n.Y")->fetch();
}
else{
	$lastdata = dibi::query("SELECT [id_polls],[name],[sent],
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=1 AND [poll]=[id_polls]) AS confirmed,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote]=1 AND [poll]=[id_polls]) AS confirmed_list,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote]=0 AND [poll]=[id_polls]) AS rejected,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick],' '),' (',COALESCE([lastname],' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote]=0 AND [poll]=[id_polls]) AS rejected_list,
		(SELECT COUNT(*) FROM [:sh:votes] WHERE [vote] IS NULL AND [poll]=[id_polls]) AS undecided,
		(SELECT GROUP_CONCAT(CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') SEPARATOR ', ') FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [vote] IS NULL AND [poll]=[id_polls]) AS undecided_list,
		(SELECT [confirmed]+[rejected]+[undecided]) AS progress_full,
		(SELECT [confirmed]/[progress_full]*100) AS progress_confirm,
		(SELECT [rejected]/[progress_full]*100) AS progress_reject,
		(SELECT [undecided]/[progress_full]*100) AS progress_undecide
		FROM [:sh:polls] ORDER BY [sent] DESC LIMIT 1")->setFormat(dibi::DATETIME,"H:i j.n.Y")->fetch();
}
$comments = dibi::query("SELECT [nick],[comment] FROM [:sh:votes] LEFT JOIN [:sh:children] ON [child]=[id_children] WHERE [comment] IS NOT NULL AND [poll]=%i",$lastdata["id_polls"]);

$template = $twig->loadTemplate('index.html');
$template->display(Array("last_data" => $lastdata,"last_comments" => $comments ));



?>
