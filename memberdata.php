<?php
require_once("lib/starter.php");
has_access();

$template = $twig->loadTemplate("memberdata.html");

$childrendata_dibi = dibi::query("SELECT * FROM [:sh:children] WHERE [id_children]=%i", $_REQUEST["child"]);
if(count($childrendata_dibi) != 1){
    my_header("members.php?err=Musíte uvést člena, pro kterého chcete zobrazit záznamy");
}
$childrendata = $childrendata_dibi->setFormat(dibi::DATE, 'Y-m-d')->fetch();


$writeuptypes = dibi::query("SELECT id_writeuptypes AS id, name FROM [:sh:writeuptypes]")->fetchAll();

if(isset($_REQUEST["onlyprotocoltype"]) and $_REQUEST["onlyprotocoltype"] != 'all'){
    $protocoldata = dibi::query("SELECT dt.id_childrendata AS id, dt.content AS content, usr.login AS creator, dt.created as created, wt.name as wtype, dt.id_writeuptypes as writeuptype
                              FROM [:sh:childrendata] dt
                              JOIN [:sh:users] usr ON usr.id_users=dt.id_users
                              JOIN [:sh:children] ch ON ch.id_children=dt.id_children
                              JOIN [:sh:writeuptypes] wt ON wt.id_writeuptypes=dt.id_writeuptypes
                              WHERE dt.id_children=%i AND dt.deleted=0 AND dt.id_writeuptypes=%i
                              ORDER BY dt.created DESC",
        $_REQUEST["child"],$_REQUEST["onlyprotocoltype"])->setFormat(dibi::DATETIME, 'j.n.Y')->fetchAssoc('id');
}
else {
    $protocoldata = dibi::query("SELECT dt.id_childrendata AS id, dt.content AS content, usr.login AS creator, dt.created as created, wt.name as wtype, dt.id_writeuptypes as writeuptype
                              FROM [:sh:childrendata] dt
                              JOIN [:sh:users] usr ON usr.id_users=dt.id_users
                              JOIN [:sh:children] ch ON ch.id_children=dt.id_children
                              JOIN [:sh:writeuptypes] wt ON wt.id_writeuptypes=dt.id_writeuptypes
                              WHERE dt.id_children=%i AND dt.deleted=0
                              ORDER BY dt.created DESC",
        $_REQUEST["child"])->setFormat(dibi::DATETIME, 'j.n.Y')->fetchAssoc('id');
}

$edit = False;
if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editwriteup"){
    $edit = $_REQUEST["protocolid"];
}

$turnout_data = dibi::query("SELECT
        (SELECT COUNT(*) FROM [:sh:votes] WHERE [child]=%i) AS invites,
        (SELECT COUNT(*) FROM [:sh:turnout] WHERE [child]=%i) AS attendance,
        [name], [place], [date_start], [date_end]
    FROM [:sh:turnout] JOIN [:sh:polls]
        ON [:sh:polls].[id_polls]=[:sh:turnout].[poll]
    WHERE [child]=%i ORDER BY date_end LIMIT 10", $_REQUEST['child'],$_REQUEST['child'],$_REQUEST['child'])->setFormat(dibi::DATETIME, 'j.n.Y')->setFormat(dibi::DATE, 'j.n.Y')->fetchAll();


$template->display(Array("protocol" => $protocoldata, "child" => $childrendata, "writeuptypes" => $writeuptypes, "edit" => $edit, "turnout" => $turnout_data));



