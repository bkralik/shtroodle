<?php
require_once("lib/starter.php");
has_access();

//Smazání writeupu
if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "deletewriteup") {
    dibi::query("UPDATE [:sh:childrendata] SET [deleted]=1 WHERE [id_childrendata]=%i", $_REQUEST["protocolid"]);
    if($_REQUEST["returnto"] == "protocol"){
        my_header("protocol.php?&ok=Záznam smazán");
    }
    my_header("memberdata.php?tab=protocol&child=".$_REQUEST["child"]."&ok=Záznam smazán");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editwriteup") {
    $result = dibi::query("UPDATE [:sh:childrendata] SET [content]=%s, [id_writeuptypes]=%i WHERE [id_childrendata]=%i AND [id_children]=%i", $_POST["writeup"],$_POST["writeuptype"],$_POST["id"],$_POST["child"]);

    my_header("memberdata.php?child=".$_POST["child"]."&tab=protocol&ok=Záznam úspěšně upraven");
    exit;

}


//Nový writeup
if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "newwriteup") {
    if(!isset($_REQUEST["child"])){
        my_header("index.php?err=Musíte editovat záznam konkrétního člena");
    }
    if(!isset($_POST["writeup"]) or empty($_POST['writeup'])){
            my_header("memberdata.php?err=Musíte vyplnit alespoň nějaký text","Must fill the writeup");
    }

    dibi::query("INSERT INTO [:sh:childrendata] ([id_children],[id_users],[content],[id_writeuptypes]) VALUES (%i,%i,%s,%i)",
        $_POST["child"],
        $_SESSION["userid"],
        $_POST["writeup"],
        $_POST["writeuptype"]
    );

    my_header("memberdata.php?tab=protocol&child=".$_POST["child"]."&ok=Záznam přidán");

}

//záznamy
if(isset($_REQUEST["action"]) and $_REQUEST["action"] == 'editchilddata'){
    $allowed_keys = Array('nick','contact_email','active','firstname','lastname','address','contact_phone','birthno','mother_name','mother_phone','father_name','father_phone','date_member','date_promise','psc','town','left_year','birthday', 'googledrive');
    $post = $_POST;
    //filter all unallowed keys
    foreach($post as $key => $value){
        if(!in_array($key, $allowed_keys)){
            unset($post[$key]);
        }
    }

    $empty_to_null = Array('birthday','date_member','date_promise','left_year');
    foreach($empty_to_null as $key){
        if(empty($post[$key])){
            $post[$key] = NULL;
        }
    }


    $result = dibi::query("UPDATE [:sh:children] SET %a WHERE [id_children]=%i",$post, $_POST["id"]);

    my_header("memberdata.php?child=".$_POST["id"]."&ok=Změny úspěšně vloženy");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == 'uploadphoto'){
    try {

        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES['upfile']['error']) ||
            is_array($_FILES['upfile']['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        // Check $_FILES['upfile']['error'] value.
        switch ($_FILES['upfile']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
                $finfo->file($_FILES['upfile']['tmp_name']),
                array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                ),
                true
            )) {
            throw new RuntimeException('Invalid file format.');
        }

        // You should name it uniquely.
        // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $filename = sprintf('./media/upload/%s.%s',
            sha1_file($_FILES['upfile']['tmp_name']),
            $ext
        );
        var_dump($filename);
        if (!move_uploaded_file(
            $_FILES['upfile']['tmp_name'],$filename
        )) {
            throw new RuntimeException('Failed to move uploaded file.');
        }

        dibi::query("UPDATE [:sh:children] SET [photo_file]=%s WHERE [id_children]=%i",$filename,$_POST["child"]);
        my_header("memberdata.php?child=".$_POST["child"]."&ok=Fotka nahrána");

    } catch (RuntimeException $e) {

        echo "Nahravani se nepovedlo. ";
        die($e->getMessage());

    }

}

my_header("members.php?err=Neplatná akce");