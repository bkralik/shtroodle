<?php
require_once("lib/starter.php");
has_access();

$edit_data = Array();

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "newmember"){
	dibi::query("INSERT INTO [:sh:children] ([nick],[contact_email]) VALUES (%s,%s)",	
	$_REQUEST["nick"],
	$_REQUEST["contact_email"]
	);
	my_header("members.php?ok=Člen úspěšně vložen");
}



if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editmember_write"){
		if(dibi::fetchSingle("SELECT COUNT([id_children]) FROM [:sh:children] WHERE [id_children]=%i",$_REQUEST["id"]) != 1){
			header("Location: members.php?err=Požadovaný člen neexistuje");
			die("Požadovaný člen neexistuje");
		}
	dibi::query("UPDATE [:sh:children] SET [nick]=%s,[contact_email]=%s WHERE [id_children]=%i",
	$_REQUEST["nick"],
	$_REQUEST["contact_email"],
	$_REQUEST["id"]
	);
	my_header("members.php?ok=Člen úspěšně upraven");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "activetoggle"){
	if(dibi::fetchSingle("SELECT COUNT([id_children]) FROM [:sh:children] WHERE [id_children]=%i",$_REQUEST["id"]) != 1){
		header("Location: members.php?err=Požadovaný člen neexistuje");
		die("Požadovaný člen neexistuje");
	}
	dibi::query("UPDATE [:sh:children] SET [active] = ![active] WHERE [id_children]=%i", $_REQUEST["id"]);
	my_header("members.php?ok=Členství bylo úspěšně změněno");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editmember" && isset($_REQUEST["id"])){
	$edit_data = dibi::fetch("SELECT [id_children],[nick],[contact_email] FROM [:sh:children] WHERE [id_children]=%i",$_REQUEST["id"]);
}

//$active_members = dibi::query("SELECT [id_children] AS id, CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') AS přezdívka, [contact_email] AS email FROM [:sh:children] WHERE [active]=1");
$active_members = dibi::query("SELECT [id_children] AS id, CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') AS přezdívka, [contact_email] AS email, [birthday] AS narozeniny, CONCAT(timestampdiff(YEAR,[birthday],NOW()),' + ',mod(timestampdiff(MONTH,[birthday],NOW()),12),' měsíců') AS věk FROM [:sh:children] WHERE [active]=1 ORDER BY narozeniny")->setFormat(dibi::DATE, 'j.n.Y');
$inactive_members = dibi::query("SELECT [id_children] AS id, CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') AS přezdívka, [contact_email] AS email FROM [:sh:children] WHERE [active]=0");


$template = $twig->loadTemplate("members.html");
$template->display(Array("active_members" => $active_members, "inactive_members" => $inactive_members, "edit" => $edit_data));


?>
