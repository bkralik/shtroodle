<?php
require_once("lib/starter.php");
has_access();

$mandatory_args = Array("subject","content");
$last_plains = dibi::query("SELECT id_plains AS id, subject AS předmět, LEFT(content,255) AS obsah,
  ABS(((ISNULL(file1) + ISNULL(file2) + ISNULL(file3))-3)) AS přílohy, sent AS odesláno
  FROM [:sh:plains] ORDER BY [id_plains] DESC LIMIT 5")->setFormat(dibi::DATE, 'j.n.Y')->setFormat(dibi::DATETIME,'H:i j.n.Y')->setFormat(dibi::TIME,'H:i')->fetchAll();
$template = $twig->loadTemplate("createplain.html");

//no action
if(!isset($_REQUEST["action"])){
    $template->display(Array("last" => $last_plains));
    exit;
}

function process_file($upfile)
{
    if($upfile["error"] != UPLOAD_ERR_OK){
        return NULL;
    }
    try {

        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($upfile['error']) ||
            is_array($upfile['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        // Check $_FILES['upfile']['error'] value.
        switch ($upfile['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        $filename = basename($upfile['name']);
        if (strpos($filename, "?") !== false) $filename = substr($filename, 0, strpos($filename, "?"));

        //change extension if it is insecure
        $insecure_ext = Array("php","js","htaccess","htpasswd", "php3", ".php4", "phtml", "pl", "py", "jsp", "asp", "sh", "cgi", "exe");
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        if(in_array($extension, $insecure_ext)){
            $filename = pathinfo($filename)["filename"] . '.txt';
        }

        //fix name
        $a = array("Ăˇ", "Ă¤", "ÄŤ", "ÄŹ", "Ă©", "Ä›", "Ă«", "Ă­", "Ĺ", "Ăł", "Ă¶", "Ĺ™", "Ĺˇ", "ĹĄ", "Ăş", "ĹŻ", "ĂĽ", "Ă˝", "Ĺľ", "Ă", "Ă„", "ÄŚ", "ÄŽ", "Ă‰", "Äš", "Ă‹", "ĂŤ", "Ĺ‡", "Ă“", "Ă–", "Ĺ", "Ĺ ", "Ĺ¤", "Ăš", "Ĺ®", "Ăś", "Ăť", "Ĺ˝");
        $b = array("a", "a", "c", "d", "e", "e", "e", "i", "n", "o", "o", "r", "s", "t", "u", "u", "u", "y", "z", "A", "A", "C", "D", "E", "E", "E", "I", "N", "O", "O", "R", "S", "T", "U", "U", "U", "Y", "Z");
        $filename = str_replace($a, $b, $filename);
        setlocale(LC_CTYPE, 'cs_CZ');
        $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
        $filename = strtolower($filename); //pokud je soubor - jmeno bezdiak
        $filename = preg_replace("/[^.a-z0-9]+/", "-", $filename);

        //to be almost sure the name is unique
        $pathinfo = pathinfo($filename);
        $filename = $pathinfo["filename"] .'-' . date('YmdHi') .'.'. $pathinfo['extension'];

        $filename = 'media/attachments/' . $filename;
        if (!        move_uploaded_file(
            $upfile['tmp_name'], $filename
        )) {
            throw new RuntimeException('Failed to move uploaded file.');
        }
        } catch (RuntimeException $e) {

        echo "Nahravani se nepovedlo. ";
        die($e->getMessage());
    }
    return $filename;
}



//------shared checks --------
$correction = Array();
if($_REQUEST["action"] == "submitplain" OR $_REQUEST["action"] == "editwrite"){
    foreach($mandatory_args as $mandatory){
		if(empty($_POST[$mandatory])){
			$correction[$mandatory] = "Položka nesmí být prázdná";
		}
	}
  if(!empty($correction)){
    $template->display(Array("last" => $last_plains, "post" => $_POST, "correction" => $correction));
      exit;
  }

}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "deleteattachment"){
    $exists = dibi::query("SELECT id_plains FROM [:sh:plains] WHERE [id_plains]=%i",$_REQUEST["id"])->fetch();
    if(empty($exists)){
        my_header("index.php?err=Takováto zpráva neexistuje!");
    }
    $change = Array("file".$_REQUEST["attachment"] => NULL);
    dibi::query("UPDATE [:sh:plains] SET %a WHERE [id_plains]=%i",$change, $_REQUEST["id"]);

    my_header("createplain.php?action=editplain&id=". $_REQUEST["id"]);
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "submitplain"){
    $file1 = process_file($_FILES["file1"]);
    $file2 = process_file($_FILES["file2"]);
    $file3 = process_file($_FILES["file3"]);
    dibi::query("INSERT INTO [:sh:plains] ([subject], [content], [file1], [file2], [file3]) VALUES (%s,%s,%s,%s,%s)", $_POST["subject"], $_POST["content"], $file1, $file2, $file3);
    my_header("submitplain.php?id=" . dibi::InsertId());
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editwrite" and $_REQUEST["id"]){
    isset($_FILES["file1"]) ? $file1 = process_file($_FILES["file1"]) : $file1 = NULL;
    isset($_FILES["file2"]) ? $file2 = process_file($_FILES["file2"]) : $file2 = NULL;
    isset($_FILES["file3"]) ? $file3 = process_file($_FILES["file3"]) : $file3 = NULL;
    dibi::query("UPDATE [:sh:plains] SET [subject]=%s,[content]=%s,
                      [file1]=COALESCE(%s, [file1]),
                      [file2]=COALESCE(%s, [file2]),
                      [file3]=COALESCE(%s, [file3])
                WHERE [id_plains]=%i", $_REQUEST["subject"], $_REQUEST["content"], $file1, $file2, $file3, $_REQUEST["id"]);
    my_header("submitplain.php?id=" . $_REQUEST["id"]);
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editplain" and $_REQUEST["id"]){
  $data = dibi::query("SELECT [id_plains] AS id,[subject],[content], file1, file2, file3, [sent] FROM [:sh:plains] WHERE [id_plains]=%i",$_REQUEST["id"])->setFormat(dibi::DATE, 'j.n.Y')->setFormat(dibi::DATETIME,'H:i j.n.Y')->fetch();
  if(empty($data)){
    my_header("index.php?err=Takováto zpráva neexistuje!");
  }
  $template->display(Array("post" => $data));
}


?>