<?php
require_once("lib/starter.php");

$template = $twig->loadTemplate('vote.html');

if(isset($_REQUEST["key"])){
	$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:votes] WHERE [key]=%s",$_REQUEST["key"]);
	if($exists == 0){
		my_header("vote.php?err=Klíč neexistuje");
	}
	if($exists > 1){
		die("Multiple keys exist! Stopping. <br>Tell this to your administrator");
	}
}
if($_REQUEST["action"] == "submitcomment" && isset($_REQUEST["key"])){
	dibi::query("UPDATE [:sh:votes] SET [comment]=%sN WHERE [key]=%s",$_REQUEST["comment"],$_REQUEST["key"]);
	my_header("vote.php?key=".$_REQUEST["key"]."&ok=Poznámka úspěšně zapsána");
}

if(isset($_REQUEST["vote"]) and isset($_REQUEST["key"])){
	dibi::query("UPDATE [:sh:votes] SET [vote]=%i WHERE [key]=%s",$_REQUEST["vote"], $_REQUEST["key"]);
	if(isset($_REQUEST["return"])){
		my_header($_REQUEST["return"]."&ok=Hlasování zaznamenáno");
	}
	my_header("vote.php?key=".$_REQUEST["key"]."&ok=Vaše rozhodnutí bylo zaznamenáno");
}

if(isset($_REQUEST["key"])){
	$key_data = dibi::query("SELECT [key],[poll],[child],[vote],[comment],[created],[updated] FROM [:sh:votes] WHERE [key]=%s",$_REQUEST["key"])->setFormat(dibi::DATETIME,'H:i j.n.Y')->fetch();
	$poll_data = dibi::query("SELECT * FROM [:sh:polls] WHERE [id_polls]=%i", $key_data["poll"])->setFormat(dibi::DATE,'j.n.Y')->setFormat(dibi::TIME,'H:i')->fetch();
    $poll_data = remake_equipment($poll_data);
	$nick = dibi::fetchSingle("SELECT [nick] FROM [:sh:children] WHERE [id_children]=%i", $key_data["child"]);

	$url = WEBURL.'vote.php?key='.$_REQUEST["key"].'&vote=';
	$participant = Array("nick" => $nick, "url_confirm" => $url.'1', "url_reject" => $url.'0');
	$template->display(Array("poll" => $poll_data, "key" => $key_data, "vote" => $key_data["vote"], "participant" => $participant ));
	exit;
}
$template->display(Array());




