<?php
require_once("lib/starter.php");
has_access();

$template = $twig->loadTemplate("createpoll.html");
$equipment = equipment_assoc();

$mandatory_args = Array("eventname","date_start", "date_end", "place", "time_start", "time_end", "place_start", "place_end");



if(isset($_REQUEST["action"]) and ($_REQUEST["action"] == "newpoll" or $_REQUEST["action"] == "editwrite")){
	$correction = Array();
	//check mandatory args existence and not-emptiness
	foreach($mandatory_args as $mandatory){
		if(empty($_POST[$mandatory])){
			$correction[$mandatory] = "Položka nesmí být prázdná";
		}
	}
	//checking sanity of dates
	list($y, $m, $d) = explode('-', $_POST["date_start"]);
	if(!checkdate($m, $d, $y)){
		$correction["date_start"] = "Datum je v chybném formátu";
	}
	list($y, $m, $d) = explode('-', $_POST["date_end"]);
	if(!checkdate($m, $d, $y)){
		$correction["date_end"] = "Datum je v chybném formátu";
	}
	if(!strtotime($_POST["time_start"]) and !isset($correction["time_start"])){
		$correction["time_start"] = "Položka nebyla vyplněna v platném formátu (HH:mm)";
	}
	if(!strtotime($_POST["time_end"]) and !isset($correction["time_end"])){
		$correction["time_end"] = "Položka nebyla vyplněna v platném formátu (HH:mm)";
	}
	
	if(!empty($correction)) {
		//ugly, but hey, it works!
		$_REQUEST["err"] = "Ne všechno bylo správně vyplněno";
		$template->display(Array(
			"equipment" => $equipment,
			"post" => $_POST,
			"correction" => $correction
			));
		exit;
	}
	if($_REQUEST["action"] == "editwrite"){
		if(!isset($_REQUEST["id"])){
			my_header("createpoll.php?err=Nelze editovat bez správného ID","Can't edit without ID");
		}
		dibi::query("UPDATE [:sh:polls] SET [name]=%s,[date_start]=%s,[date_end]=%s,[time_start]=%s,[time_end]=%s,[place]=%s,[place_start]=%s,[place_end]=%s,[price]=%iN,[equipment]=%s,[other_equipment]=%sN,[comment]=%sN WHERE [id_polls]=%i", 
		$_POST["eventname"],
		$_POST["date_start"], 
		$_POST["date_end"], 
		$_POST["time_start"], 
		$_POST["time_end"], 
		$_POST["place"], 
		$_POST["place_start"], 
		$_POST["place_end"], 
		$_POST["price"], 
		empty($_POST["equipment"]) ? null : implode(";",$_POST["equipment"]), 
		$_POST["other_equipment"],  
		$_POST["comment"],
		$_REQUEST["id"]
		);	
		my_header("submitpoll.php?ok=Úspěšně upraveno&id=".$_REQUEST["id"]);
		exit;
	}
	else {
		dibi::query("INSERT INTO [:sh:polls] ([name],[date_start],[date_end],[time_start],[time_end],[place],[place_start],[place_end],[price],[equipment],[other_equipment],[comment]) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%iN,%s,%sN,%sN)", 
		$_POST["eventname"],
		$_POST["date_start"], 
		$_POST["date_end"], 
		$_POST["time_start"], 
		$_POST["time_end"], 
		$_POST["place"], 
		$_POST["place_start"], 
		$_POST["place_end"], 
		$_POST["price"], 
		empty($_POST["equipment"]) ? null : implode(";",$_POST["equipment"]), 
		$_POST["other_equipment"],  
		$_POST["comment"]
		);	
		my_header("submitpoll.php?ok=Úspěšně vloženo&id=".dibi::getInsertId());
	}

}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editpoll" and isset($_REQUEST["id"])){
	$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:polls] WHERE [id_polls]=%i",$_REQUEST["id"]);
	if(!$exists){
		my_header("index.php?err=Tato pozvánka neexistuje");
	}
	$polldata = dibi::query("SELECT [name] AS eventname,[date_start],[date_end],[time_start],[time_end],[place],[place_start],[place_end],[price],[equipment],[other_equipment],[comment] FROM [:sh:polls] WHERE [id_polls]=",$_REQUEST["id"])->setFormat(dibi::DATE,'Y-m-d')->setFormat(dibi::TIME,'H:i')->fetch()->toArray();
	$polldata["equipment"] = explode(";",$polldata["equipment"]);

	$template->display(Array(
		"equipment" => $equipment,
		"post" => $polldata
		));
	exit;
}


$template->display(Array("equipment" => $equipment));


?>
