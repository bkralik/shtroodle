<?php
require_once("lib/starter.php");

require_once("lib/phpmailer/PHPMailerAutoload.php");

$remove_email = $_GET['email'];

$mail = new PHPMailer();
$mail->CharSet = 'UTF-8';
$mail->isHTML(true);

if($sh_config["smtp_on"]){
    $mail->isSMTP();
    $mail->Host = $sh_config["smtp_host"];
    $mail->Port = $sh_config["smtp_port"]; //587 for Google
    $mail->SMTPAuth = true;
    $mail->Username = $sh_config["smtp_user"];
    $mail->Password = $sh_config["smtp_pass"];
    $mail->SMTPSecure = 'tls';
}
else{
    $mail->isMail();
}

$mail->setFrom(strip_tags($sh_config["send_emails_from"]));
$mail->addAddress(strip_tags($sh_config["send_emails_from"]));
$mail->Subject = $sh_config["send_emails_subject"]." někdo se chce odhlásit";
$mail->Body = "Tady je Štrůdl,

rodič s emailem " . $remove_email . "odklikl, že nechce dále dostávat maily.

Dělej si s tím co chceš
ŠtrůdlBot";

$mail->send();
//dibi::query("UPDATE [:sh:children] SET active=0 WHERE contact_email=%s", $email);

my_header("index.php?ok=Vedení oddílu dostalo informaci o tom, že se chcete odhlásit");

?>