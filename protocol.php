<?php

require_once("lib/starter.php");
has_access();

$template = $twig->loadTemplate("protocol.html");
if(isset($_REQUEST["onlyprotocoltype"]) and $_REQUEST["onlyprotocoltype"] != 'all'){
    $protocoldata = dibi::query("SELECT dt.id_childrendata AS id, dt.content AS content, usr.login AS creator, dt.created as created, wt.name as wtype, dt.id_writeuptypes as writeuptype, ch.nick as nick, ch.lastname as lastname, dt.id_children as id_children
                              FROM [:sh:childrendata] dt
                              JOIN [:sh:users] usr ON usr.id_users=dt.id_users
                              JOIN [:sh:children] ch ON ch.id_children=dt.id_children
                              JOIN [:sh:writeuptypes] wt ON wt.id_writeuptypes=dt.id_writeuptypes
                              WHERE dt.deleted=0 AND dt.id_writeuptypes=%i", $_REQUEST["onlyprotocoltype"])->setFormat(dibi::DATETIME, 'j.n.Y')->fetchAll();
}
else {
    $protocoldata = dibi::query("SELECT dt.id_childrendata AS id, dt.content AS content, usr.login AS creator, dt.created as created, wt.name as wtype, dt.id_writeuptypes as writeuptype, ch.nick as nick, ch.lastname as lastname, dt.id_children as id_children
                              FROM [:sh:childrendata] dt
                              JOIN [:sh:users] usr ON usr.id_users=dt.id_users
                              JOIN [:sh:children] ch ON ch.id_children=dt.id_children
                              JOIN [:sh:writeuptypes] wt ON wt.id_writeuptypes=dt.id_writeuptypes
                              WHERE dt.deleted=0
                              ORDER BY dt.created DESC")->setFormat(dibi::DATETIME, 'j.n.Y')->fetchAll();
}

$writeuptypes = dibi::query("SELECT id_writeuptypes AS id, name FROM [:sh:writeuptypes]")->fetchAll();

$template->display(Array("protocol" => $protocoldata, "writeuptypes" => $writeuptypes));