<?php
require_once("lib/starter.php");
has_access();

if($_REQUEST["action"] == "changepass"){
	if($_REQUEST["passwd1"] != $_REQUEST["passwd2"]){
		my_header("user.php?err=Hesla se neshodují!&mt=alert-error");
	}
	dibi::query("UPDATE [:sh:users] SET [passwd]=SHA1(%s) WHERE [id_users]=%i",$_REQUEST["passwd1"],$_SESSION["userid"]);
	my_header("user.php?ok=Heslo bylo změněno");
	
}

if($_REQUEST["action"] == "changeemail"){
	dibi::query("UPDATE [:sh:users] SET [email]=%s WHERE [id_users]=%i",$_REQUEST["email"],$_SESSION['userid']);
	$_SESSION["email"] = $_REQUEST["email"];
	my_header("user.php?ok=Email úspěšně změněn");
}
if($_REQUEST["action"] == "changenick"){
	dibi::query("UPDATE [:sh:users] SET [nick]=%s WHERE [id_users]=%i",$_REQUEST["nick"],$_SESSION['userid']);
	$_SESSION["nick"] = $_REQUEST["nick"];
	my_header("user.php?ok=Přezdívka úspěšně změněna");
}


$template = $twig->loadTemplate("user.html");
$template->display(Array());

?>
