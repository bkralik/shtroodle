<?php
require_once("lib/starter.php");
has_access();

$template = $twig->loadTemplate("groups.html");

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "deletegroup" and isset($_REQUEST["id"])){
	dibi::query("DELETE FROM [:sh:groups] WHERE [id_groups]=%i",$_REQUEST["id"]);
	if (dibi::affectedRows() == 0){
		my_header("groups.php?warn=Zadaná skupina neexistuje","Group does not exist");
	}
	else {
		my_header("groups.php?ok=Úspěšně smazáno");
	}
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "newgroup"){
	if(!isset($_REQUEST["name"]) or empty($_REQUEST["name"])){
		my_header("groups.php?err=Jméno musí být vyplněno", "Name of group must be filled in");
	}

	dibi::begin();
	if( dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:groups] WHERE [name]=%s",$_REQUEST["name"]) != 0 ){
		dibi::rollback();
		my_header("groups.php?tab=new&err=Skupina s tímto jménem už existuje", "Group with such name exists");
	}

	dibi::query("INSERT INTO [:sh:groups] ([name]) VALUES (%s)",$_REQUEST["name"]);
	$group_id = dibi::getInsertId();
	if(!empty($_REQUEST["newmembers"])){
		foreach ($_REQUEST["newmembers"] as $member){
			dibi::query("INSERT INTO [:sh:group_members] ([group],[child]) VALUES (%i,%i)",$group_id, $member);
		}
	}
	dibi::commit();
	my_header("groups.php?ok=Skupina úspěšně vytvořena");
}


if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editmembers" && isset($_REQUEST["id"])){
	dibi::begin();

	if( dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:groups] WHERE [name]=%s",$_REQUEST["name"]) != 0 ){
		dibi::rollback();
		my_header("groups.php?tab=new&err=Skupina neexistuje", "Group doesn't exists");
	}

	dibi::query("DELETE FROM [:sh:group_members] WHERE [group]=%i",$_REQUEST["id"]);
	if(!empty($_REQUEST["newmembers"])){
		foreach($_REQUEST["newmembers"] as $member){
			dibi::query("INSERT INTO [:sh:group_members] ([group],[child]) VALUES (%i,%i)", $_REQUEST["id"],$member);
		}
	}
	dibi::commit();
	my_header("groups.php?tab=members&ok=Členové skupiny úspěšně nastaveni");
}


$overview_data = dibi::query("SELECT [id_groups] AS id, [name] AS jméno,(SELECT COUNT(*) FROM [:sh:group_members] WHERE [group]=[id_groups]) AS 'počet osob', (SELECT GROUP_CONCAT([nick] SEPARATOR ', ') FROM [:sh:group_members] AS gm LEFT JOIN [:sh:children] ON [id_children]=[child] WHERE [gm].[group]=[:sh:groups].[id_groups]) AS členové FROM [:sh:groups]");
$allmembers = dibi::query("SELECT [id_children] AS id, CONCAT(COALESCE([nick], ' '),' (',COALESCE([lastname], ' '),')') AS nick FROM [:sh:children] WHERE [active]=1");
$group = dibi::query("SELECT [id_groups] AS [id],[name], (SELECT GROUP_CONCAT([child]) FROM [:sh:group_members] WHERE [group]=[id_groups] GROUP BY [group]) AS members FROM [:sh:groups]");

$template->display(Array(
	"overview_data" => $overview_data,
	"allmembers" => $allmembers,
	"groups" => $group,
));
