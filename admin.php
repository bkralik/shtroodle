<?php
require_once("lib/starter.php");
has_access(1);

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "makeclean"){
	dibi::begin();
	dibi::query("TRUNCATE TABLE [:sh:polls]");
	dibi::query("TRUNCATE TABLE [:sh:votes]");
	dibi::commit();
	my_header("index.php?ok=Systém byl vyčištěn");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "activetoggle" and isset($_REQUEST["id"])){
	dibi::query("UPDATE [:sh:users] SET [active] = ![active] WHERE [id_users]=%i",$_REQUEST["id"]);
	my_header("admin.php?tab=users&ok=Aktivita uživatele změněna");
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "newuser"){
	dibi::begin();
	$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:users] WHERE [login]=%s",$_REQUEST["login"]);
	if($exists){
		my_header("admin.php?err=Tento login již existuje");
	}
	try{
		dibi::query("INSERT INTO [:sh:users] ([nick],[login],[passwd],[email],[active],[superadmin], [canapprove]) VALUES (%sN,%sN,SHA1(%sN),%sN,%i,%i,%i)",
		$_REQUEST["nick"],
		$_REQUEST["login"],
		$_REQUEST["password"],
		$_REQUEST["email"],
		1,
		isset($_REQUEST["superadmin"]) ? 1 : 0,
        1 //canapprove
		);
	} catch (DibiDriverException $e){
		dibi::rollback();
		my_header("admin.php?err=Uživatel nebyl vytvořen! Pravděpodobně chybné/prázdné parametry.");
	}
	dibi::commit();
	my_header("admin.php?ok=Uživatel vytvořen");
}


if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "edituser" && isset($_REQUEST["id"])){
	$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:users] WHERE [id_users]=%i",$_REQUEST["id"]);
	if(!$exists){
		my_header("admin.php?err=Editovaný uživatel neexistuje!");
	}
	$edit_data = dibi::fetch("SELECT [id_users] AS id,[nick],[login],[email],[active],[superadmin] FROM [:sh:users] WHERE [id_users]=%i",$_REQUEST["id"]);
}




if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "edituser_write" && isset($_REQUEST["id"])){
	dibi::begin();
	$exists = dibi::fetch("SELECT COUNT(*) FROM [:sh:users] WHERE [id_users]=%i",$_REQUEST["id"]);
	if(!$exists){
		my_header("admin.php?err=Editovaný uživatel neexistuje!");
	}
	if(!dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:users] WHERE [id_users]!=%i AND [login]=%s",$_REQUEST["id"],$_REQUEST["login"])){
		dibi::query("UPDATE [:sh:users] SET [nick]=%s,[login]=%s,[email]=%s,[active]=%i,[superadmin]=%i WHERE [id_users]=%i",
			$_REQUEST["nick"],
			$_REQUEST["login"],
			$_REQUEST["email"],
			isset($_REQUEST["active"]) ? 1 : 0,
			isset($_REQUEST["superadmin"]) ? 1 : 0,
			$_REQUEST["id"]
		);
		if(!empty($_REQUEST["password"])){
			dibi::query("UPDATE [:sh:users] SET [passwd]=SHA1(%s) WHERE [id_users]=%i",$_REQUEST["password"],$_REQUEST["id"]);
		}
		dibi::commit();
		my_header("admin.php?ok=Uživatel úspěšně změněn");
	}
	else {
		dibi::rollback();
		print_r($_POST);
		$edit_data = $_POST;
		$_REQUEST["tab"] = "edit";
		$correction = Array("login" => "Tento login již existuje");
	}
}

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editconfig"){
    $update_arr = $_POST;
    //checkbox solution
    if(!isset($_POST["smtp_on"])){
        $update_arr["smtp_on"] = '';
    }
    //if smtp_pass is empty, don't change it
    if(empty($update_arr['smtp_pass'])){
        unset($update_arr["smtp_pass"]);
    }

    foreach($update_arr as $key => $value){
		dibi::query("UPDATE [:sh:config] SET [value]=%s WHERE [key]=%s",$value,$key);
	}
	my_header("admin.php?tab=config&ok=Nastavení úspěšně změněno");
	exit;
}

//Load data
$users_data = dibi::query("SELECT [id_users] AS id, [nick] AS přezdívka, [login], [email], [active] AS aktivní,[superadmin] FROM [:sh:users]");

$template_data = Array(
    "users_data" => $users_data,
    "config" => $sh_config
);
if(isset($edit_data)){
    $template_data["edit"] = $edit_data;
}
if(isset($correction)){
    $template_data["correction"] = $correction;
}

$template = $twig->loadTemplate('admin.html');
$template->display($template_data);
