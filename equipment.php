<?php
require_once("lib/starter.php");
has_access();

$edit = Array();
$template = $twig->loadTemplate("equipment.html");

if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "editwrite"){
	dibi::query("UPDATE [:sh:equipment] SET [text]=%s WHERE [keyword]=%s",$_REQUEST["text"],$_REQUEST["oldkeyword"])	;
	my_header("equipment.php?ok=Úspěšně upraveno");
}



if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "newwrite"){
	dibi::begin();
	try{
		$keyword = $_REQUEST["text"];
		$keyword = str_replace(";","",$keyword);
		$keyword = urlify($keyword);
		if(strlen($keyword) == 0){
			die($keyword);
			my_header("equipment.php?tab=new&err=Chybný klíč","Incorrect keyword submitted");
		}
		while(True){
			$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:equipment] WHERE [keyword]=%s",$keyword);
			if(!$exists){
				break;
			}
			$keyword .= rand(0,9);
		}
		
		dibi::query("INSERT INTO [:sh:equipment] ([keyword],[text]) VALUES(%s,%s)",$keyword,$_REQUEST["text"]);
	}
	catch(DibiDriverException $e){
		dibi::rollback();
		my_header("equipment.php?tab=new&err=Přidání selhalo (pravděpodobně vkládáte klíč, který již existuje)", "Error when inserting, duplicate key");
	}
	dibi::commit();
	my_header("equipment.php?ok=Úspěšně přidáno");
}



if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "hidetoggle"){
	dibi::query("UPDATE [:sh:equipment] SET [hidden]=[hidden]<>1 WHERE [keyword]=%s",$_REQUEST["id"]);
	my_header("equipment.php?ok=Zobrazit/skrýt bylo úspěšně změněno");
}


$overview_data = dibi::query("SELECT [keyword] AS 'klíč', [text], [hidden] AS schováno FROM [:sh:equipment]");

if(isset($_REQUEST["action"]) and isset($_REQUEST["edit"])){
	$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:equipment] WHERE [keyword]=%s",$_REQUEST["edit"]);
	if(!$exists){
		my_header("equipment.php?err=Toto vybavení neexistuje");
	}
	$edit = dibi::fetch("SELECT [keyword],[keyword] AS oldkeyword, [text] FROM [:sh:equipment] WHERE [keyword]=%s",$_REQUEST["edit"]);
}

$template->display(Array(
	"overview_data" => $overview_data,
	"edit" => $edit,
));
