<?php
function my_header($location,$error_msg=null){
	header("Location: ".$location);
	if(is_null($error_msg)){
		exit;
	}
	else {
		die($error_msg);
	}
}

function generate_key($email,$id){
	$random = rand();
	return substr( base64_encode(md5(sha1($email.$id.$random.time()))),0,16);
}

function equipment_assoc(){
	return dibi::query("SELECT [keyword],[text] FROM [:sh:equipment] WHERE [hidden]=0")->fetchPairs("keyword","text");
}

function remake_equipment($poll_data){
    $equip_translate = equipment_assoc();
    $equipment_str = '';
    foreach(explode(';',$poll_data["equipment"]) as $single_equip){
        $equipment_str .= $equip_translate[$single_equip].', ';
    }
    if(empty($poll_data["other_equipment"])){
        $equipment_str = rtrim($equipment_str, ', ');
    }
    else {
        $equipment_str .= $poll_data["other_equipment"];
    }
    $equipment_str = trim($equipment_str,',');
    $poll_data["equipment_str"] = $equipment_str;
    return $poll_data;
}

function has_access($superadmin=null){
	if($_SESSION["auth"] != 1){
		my_header("login.php?warn=Musíte se přihlásit!");
	}
	if(!is_null($superadmin)){
		if($_SESSION["superadmin"] != 1){
			my_header("index.php?err=Musíte být superadmin!");
		}
	}
}

//From: http://php.vrana.cz/vytvoreni-pratelskeho-url.php
function urlify($text) {
    $url = $text;
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
    return $url;
}

?>
