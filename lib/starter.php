<?php
define('DBVERSION',4);
define('DOCROOT',dirname(__FILE__).'/../');
date_default_timezone_set('Europe/Prague');


if(file_exists('config.php')){
	require_once(DOCROOT.'config.php');
	require_once(DOCROOT.'dibi/dibi.php');
	dibi::connect(array(
	    'driver'   => 'mysql',
	    'host'     => $sh_config["db_host"],
	    'username' => $sh_config["db_user"],
	    'password' => $sh_config["db_pass"],
	    'database' => $sh_config["db_database"],
	    'charset'  => 'utf8',
	));
	//mark
	dibi::getSubstitutes()->sh = $sh_config["table_prefix"];
}

setlocale(LC_ALL, 'en_US.UTF8');

//let's start our session
function startSession($time = 86400, $ses = 'MYSES') {
	session_set_cookie_params($time);
	ini_set('session.gc_maxlifetime', $time);
	ini_set('session.lifetime',$time);

        session_name($ses);
	session_start();
	// Reset the expiration time upon page load
	if (isset($_COOKIE[$ses])){
		setcookie($ses, $_COOKIE[$ses], time() + $time, "/");
		session_regenerate_id();
	}
    if(!isset($_SESSION["auth"])){
        $_SESSION["auth"] = 0;
    }
}
startSession();

require_once(DOCROOT.'lib/dbversion.php');
dbv_check_version(DBVERSION);

require_once(DOCROOT.'Twig/Autoloader.php');
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(DOCROOT.'templates');
$twig = new Twig_Environment($loader,Array("debug" => true));
$twig->addExtension(new Twig_Extension_Debug());
require_once(DOCROOT.'lib/functions.php');

//load config
$config_data = dibi::query("SELECT [key],[value] FROM [:sh:config]")->fetchPairs('key','value');
foreach($config_data as $key => $value){
	$sh_config[$key] = $value;
}
unset($config_data);

define('WEBURL',$sh_config["web_url"]);


?>
