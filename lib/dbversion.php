<?php
require_once("dibi/dibi.php");

/*
 * Redefine this function for your configuration
 * Takes SQL query, executes it and return first field of first row (result[0][0])
 */
class OldDBVersionError extends Exception {}

function dbv_sql($sql){
    return dibi::query($sql)->fetchSingle();
}

function dbv_check_version($expected){
    $current = dbv_sql("SELECT MAX(version) FROM sh_dbversion");
    if(intval($expected) > intval($current)){
        throw new OldDBVersionError("Current version: ".$current." expected: ".$expected);

    }
    return True;

}