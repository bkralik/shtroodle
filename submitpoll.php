<?php
require_once("lib/starter.php");
has_access();

require_once("lib/phpmailer/PHPMailerAutoload.php");

if(!isset($_REQUEST["id"])){
	my_header("index.php?warn=Nelze potvrdit nezadanou akci","Must supply ID");
}
if(dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:polls] WHERE [id_polls]=%i",$_REQUEST["id"]) != 1){
	my_header("index.php?err=Toto hlasování neexistuje","ID invalid");
}

//loading poll data
$poll_data = dibi::query("SELECT * FROM [:sh:polls] WHERE [id_polls]=%i",$_REQUEST["id"])->setFormat(dibi::DATE, 'j.n.Y')->setFormat(dibi::DATETIME,'H:i j.n.Y')->setFormat(dibi::TIME,'H:i')->fetch();
$poll_data = remake_equipment($poll_data);

/******** action sendpoll ***********/
if(isset($_REQUEST["action"]) and $_REQUEST["action"] == "sendpoll"){

	if(!isset($_REQUEST["sendgroup"])){
		my_header("submitpoll.php?err=Musíte vybrat skupinu, které bude pozvání odesláno&id=".$_REQUEST["id"],"No group supplied");
	}
	//get recipients (children IDs, nicks and emails)
	//using GROUP_CONCAT for finding siblings and sending 1 mail with multiple links
	if($_REQUEST["sendgroup"] == "all"){
		$recipients = dibi::query("SELECT GROUP_CONCAT([id_children]) AS ids, GROUP_CONCAT([nick]) AS nicks, [contact_email] FROM [:sh:children] WHERE [active]=1 GROUP BY [contact_email]");
	}
	else{
		$recipients = dibi::query("SELECT GROUP_CONCAT([id_children]) AS ids, GROUP_CONCAT([nick]) AS nicks,[contact_email] FROM [:sh:children] WHERE [active]=1 AND [id_children] IN (SELECT [child] FROM [:sh:group_members] WHERE [group]=%i) GROUP BY [contact_email]",$_REQUEST["sendgroup"]);
	}
	if(count($recipients) == 0){
		my_header("submitpoll.php?err=Zadaná skupina nemá nikoho, komu by šel poslat email&id=".$_REQUEST["id"],"No emails in selected group");
	}


	$failed_recipients = Array();
    $err_info = Array();
	
	foreach($recipients as $recipient){ //recipient here means email address (more participants may have same address (siblings))
		dibi::begin();
		$template_data = Array();  //data for template generations

		$children = array_combine(explode(',',$recipient["ids"]), explode(',', $recipient["nicks"]));
		foreach( $children as $ch_id => $nick){
			
			$exists = dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:votes] WHERE [poll]=%i AND [child]=%i",$_REQUEST["id"],$ch_id);
			if(!$exists){
				//making sure there will be no duplicities
				$key = generate_key($recipient["contact_email"],$_REQUEST["id"]);
				try {
					dibi::query("INSERT INTO [:sh:votes] ([poll],[child],[key],[created],[updated]) VALUES (%i,%i,%s,%sN,%sN)",$_REQUEST["id"],$ch_id, $key, null, null);
				}
				catch(DibiDriverException $exception){
					while(True){
						//generate_key is time-related, so every key should be different
						$key = generate_key($recipient["contact_email"],$_REQUEST["id"]);
						if(dibi::fetchSingle("SELECT COUNT(*) FROM [:sh:votes] WHERE [key]=%s",$key) == 0){
							//found our desired key
							break;
						}
					}
					dibi::query("INSERT INTO [:sh:votes] ([poll],[child],[key],[created],[updated]) VALUES (%i,%i,%s,%sN,%sN)",$_REQUEST["id"],$ch_id, $key, null, null);
				}
			}
			else{
				$key = dibi::fetchSingle("SELECT [key] FROM [:sh:votes] WHERE [poll]=%i AND [child]=%i",$_REQUEST["id"],$ch_id);
			}
			
			$url = WEBURL.'vote.php?key='.$key.'&vote=';
			$template_data[] = Array("nick" => $nick, "url_confirm" => $url.'1', "url_reject" => $url."0");
		}
		
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);

        if($sh_config["smtp_on"]){
            $mail->isSMTP();
            $mail->Host = $sh_config["smtp_host"];
            $mail->Port = $sh_config["smtp_port"]; //587 for Google
            $mail->SMTPAuth = true;
            $mail->Username = $sh_config["smtp_user"];
            $mail->Password = $sh_config["smtp_pass"];
            $mail->SMTPSecure = 'tls';
        }
        else{
            $mail->isMail();
        }

        $mail->setFrom(strip_tags($sh_config["send_emails_from"]));
		$mail->addReplyTo(strip_tags($sh_config["send_emails_reply_to"]));
        //add recipients
        foreach(explode(',',$recipient["contact_email"]) as $single_rec){
            $mail->addAddress($single_rec);
        }
        //if bcc is enabled
        if(!empty($sh_config["bcc_to"])){
            foreach(explode(',',$sh_config["bcc_to"]) as $single_rec){
                $mail->addBCC($single_rec);
            }
        }

		$mail->Subject = $sh_config["send_emails_subject"]." ".$poll_data["name"];

        $template = $twig->loadTemplate("mail.html");
        $mail->Body = $template->render(Array("poll_data" => $poll_data, "participants" => $template_data, "domain_url" => WEBURL, "email"=>$recipient["contact_email"]));

        $alt_template = $twig->loadTemplate("mail.txt");
        $mail->AltBody = $alt_template->render(Array("data" => $poll_data, "participants" => $template_data));

        $mail_return = $mail->send();

		if($mail_return){
			dibi::commit();
		}
		else{
			dibi::rollback();
			$failed_recipients[] = $recipient["contact_email"];
            $err_info[] = $mail->ErrorInfo;
		}

	}
	dibi::query("UPDATE [:sh:polls] SET [sent]=NOW() WHERE [id_polls]=%i",$_REQUEST["id"]);
	if(empty($failed_recipients)){
		my_header("index.php?ok=Pozvánka úspěšně odeslána");
	}
	else{
		my_header("index.php?err=Odeslání selhalo pro následující emaily: ".implode(', ',$failed_recipients) . "because: ".implode(', ',$err_info),"Some email sending failed".implode(', ',$failed_recipients) . "because: ".implode(', ',$err_info));
	}
}
//end of sendpoll action




$groups = dibi::query("SELECT [id_groups],[name] FROM [:sh:groups]");

//render template
$template = $twig->loadTemplate("submitpoll.html");
$template->display(Array("poll_data" => $poll_data, "groups" => $groups));
