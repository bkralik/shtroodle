<?php
require_once("lib/starter.php");
has_access();

require_once("excel/PHPExcel.php");
require_once("excel/PHPExcel/IOFactory.php");

function get_children_dibi($id){
    return dibi::query("SELECT [id_children],[firstname],[lastname],[birthday],[address],[town],[psc],[birthday] FROM [:sh:turnout] JOIN [:sh:children] ON [:sh:turnout].[child]=[id_children] WHERE [:sh:turnout].[poll]=%i", $id)->setFormat(dibi::DATE,'j.n.Y')->fetchAll();
}

function get_event_details($id){
    return dibi::query("SELECT [name],[place],[date_start],[date_end] FROM [:sh:polls] WHERE [id_polls]=%i",$id)->setFormat(dibi::DATE,'j.n.Y')->fetch();
}

function export_participation($id, $event_details){
    /** Get data **/

    $children_dibi = get_children_dibi($id);

    /** Some excel stuff **/
    $styles_bold = Array(
        'font' => Array(
            'bold' => true
        ));
    $styles_thinborder = array(
        'borders' => array(
            'outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THICK
            )
        )
    );
    /** Start generating doc with participation **/
    $xls_doc = PHPExcel_IOFactory::load('excel/people-template.xls');
    $xls_doc->setActiveSheetIndex(0);
    $xls_list = $xls_doc->getActiveSheet();

    /** Insert event name **/
    $xls_list->setCellValueByColumnAndRow(2,7,$event_details["name"]);
    /** What is our first edited column **/
    $start_column = 0;

    /** Filling in children **/
    $excel_row = 9;
    $i = 1;
    foreach($children_dibi as $row){
        $excel_column = $start_column;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$i);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["firstname"]);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["lastname"]);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["birthday"]);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["address"]);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["town"]);
        $excel_column++;
        $xls_list->setCellValueByColumnAndRow($excel_column,$excel_row,$row["psc"]);
        $excel_row++;
        $i++;
    }
    /** Appending last line **/
    //Fucking fix: In case there are no children Excel dies on SUM(H9:H9)
    if($excel_row == "9"){
        $excel_row++;
    }
    $xls_list->setCellValueByColumnAndRow(4,$excel_row, 'Celkem');
    $xls_list->getStyleByColumnAndRow(4,$excel_row)->applyFromArray($styles_bold);
    $xls_list->setCellValueByColumnAndRow(7,$excel_row, '=SUM(H9:H'.($excel_row-1).')');
    $xls_list->getStyleByColumnAndRow(7,$excel_row)->applyFromArray($styles_bold);

    /** Making borders **/
    $xls_list->getStyle('A9:H'.$excel_row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /** Setting doc properties **/
    $properties = $xls_doc->getProperties();
    $properties->setCreated(time());
    $properties->setCreator('CXL účto');

    /** Save it **/
    $writer = PHPExcel_IOFactory::createWriter($xls_doc,'Excel5');
    $writer->save('php://output');

    unset($xls_doc,$xls_list,$properties,$writer);

    return True;
}


$event_details = get_event_details($_REQUEST['id']);
if(empty($event_details)){
    die("Chyba!");
}

header('Cache-Control: max-age=0');
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="event-'.$event.'-items.xls"');
export_participation($_REQUEST['id'], $event_details);
exit;
